/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.9090909090909, "KoPercent": 0.09090909090909091};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8495454545454545, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "GET find pet by status"], "isController": false}, {"data": [0.925, 500, 1500, "GET find purchase order by Id"], "isController": false}, {"data": [0.915, 500, 1500, "POST Place an order for a pet"], "isController": false}, {"data": [0.94, 500, 1500, "POST add new pet"], "isController": false}, {"data": [0.91, 500, 1500, "GET find user by username"], "isController": false}, {"data": [0.92, 500, 1500, "GET user logout"], "isController": false}, {"data": [0.9525, 500, 1500, "GET find pet by Id"], "isController": false}, {"data": [0.955, 500, 1500, "POST create list user"], "isController": false}, {"data": [0.93, 500, 1500, "GET return pet inventories by status"], "isController": false}, {"data": [0.945, 500, 1500, "GET user login"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1100, 1, 0.09090909090909091, 822.9727272727276, 237, 12155, 373.5, 1580.3999999999992, 3658.55, 7007.4000000000015, 10.233700506103007, 52.72507163590354, 2.3076703911134264], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET find pet by status", 100, 0, 0.0, 4398.149999999998, 1929, 12155, 3814.0, 7760.500000000005, 8478.599999999997, 12135.919999999991, 0.9753242953281966, 51.089724882351504, 0.18001590997756753], "isController": false}, {"data": ["GET find purchase order by Id", 100, 0, 0.0, 503.18, 240, 1467, 380.5, 964.8000000000002, 1422.6, 1466.92, 1.0023253949162054, 0.43460202670194853, 0.16542284349691283], "isController": false}, {"data": ["POST Place an order for a pet", 100, 0, 0.0, 500.84999999999985, 242, 1597, 375.0, 1346.3000000000018, 1499.2499999999995, 1596.87, 1.0135306339634116, 0.4463889803881822, 0.32068742715248566], "isController": false}, {"data": ["POST add new pet", 100, 0, 0.0, 455.2900000000001, 242, 1617, 369.5, 509.70000000000016, 1436.449999999999, 1616.6999999999998, 1.0093261738463402, 0.4888923654568211, 0.3982107170253139], "isController": false}, {"data": ["GET find user by username", 100, 0, 0.0, 502.81000000000006, 237, 1533, 368.5, 1338.8000000000002, 1396.4499999999998, 1532.81, 1.0028983763075288, 0.49702233203959445, 0.16551740780856675], "isController": false}, {"data": ["GET user logout", 100, 0, 0.0, 487.3700000000001, 244, 1712, 367.5, 919.2, 1422.9999999999998, 1710.7499999999993, 1.0031700172545241, 0.37324978181052126, 0.16360292273584529], "isController": false}, {"data": ["GET find pet by Id", 200, 0, 0.0, 438.4949999999999, 238, 1850, 371.0, 460.70000000000005, 837.6499999999999, 1713.2000000000007, 2.0093838224508453, 0.7868778445339737, 0.3159285111470568], "isController": false}, {"data": ["POST create list user", 100, 0, 0.0, 437.23000000000013, 241, 1514, 368.0, 476.0, 1389.5, 1512.8799999999994, 1.0025163159530421, 0.3730065589629971, 0.4141253922345086], "isController": false}, {"data": ["GET return pet inventories by status", 100, 1, 1.0, 443.63, 242, 1533, 368.0, 785.4000000000019, 886.9499999999996, 1532.79, 1.014126786130802, 0.4449976453243685, 0.1693512504183273], "isController": false}, {"data": ["GET user login", 100, 0, 0.0, 447.19999999999993, 243, 1530, 372.5, 457.0, 1318.1999999999946, 1529.81, 1.0026670944712937, 0.47195853470230814, 0.1977917510578138], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500/Server Error", 1, 100.0, 0.09090909090909091], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1100, 1, "500/Server Error", 1, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["GET return pet inventories by status", 100, 1, "500/Server Error", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
